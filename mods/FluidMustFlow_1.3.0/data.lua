BASE_AREA = 4 / 5
HEIGHT = 60

require("prototypes.entities")
require("prototypes.items")
require("prototypes.recipes")
require("prototypes.technologies")
require("prototypes.tips-and-tricks")

require("compatibility-scripts/data/ab_fix")
require("compatibility-scripts/data/squeak_through_fix")
